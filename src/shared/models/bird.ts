export class Bird {
  id: number
  name: string
  image: string
  sightings: number
  seen?: boolean = false
}
