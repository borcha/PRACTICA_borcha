import { Bird } from './bird';
import { BirdSight } from './bird-sight';

export class BirdDetail extends Bird {
  description: string;
  sightingsList: BirdSight[];
}
