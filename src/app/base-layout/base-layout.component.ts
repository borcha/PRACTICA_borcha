import { Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { BirdsApiService } from '../birds-api.service'

@Component({
  selector: 'base-layout',
  templateUrl: './base-layout.component.html',
  styleUrls: ['./base-layout.component.scss']
})
export class BaseLayoutComponent implements OnInit {

  loggedUser
  @Input() loading = false
  @Input() hideBack: boolean = false;
  @Input() title: String;

  constructor(
    private location: Location,
    private router: Router,
    private birdsApi: BirdsApiService,
  ) {}

  ngOnInit() {
    this.loggedUser = this.birdsApi.getLoggedUser()
    if (this.loggedUser == null) {
      this.logout()
    }
  }

  gotoMain() {
    this.router.navigate(['/']);
    return false; // preventDefault
  }

  logout() {
    this.router.navigate(['/login']);
    return false; // preventDefault
  }

  goBack() {
    this.location.back();
    return false; // preventDefault
  }
}
