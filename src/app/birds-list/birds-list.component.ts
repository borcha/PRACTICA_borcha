import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Bird } from '../../shared/models/bird'

import { BirdsApiService } from '../birds-api.service'

@Component({
  selector: 'app-birds-list',
  templateUrl: './birds-list.component.html',
  styleUrls: ['./birds-list.component.scss']
})
export class BirdsListComponent implements OnInit {

  loading = true

  birds: Bird[]

  constructor(
    private router: Router,
    private birdsApi: BirdsApiService,
  ) {}

  ngOnInit() {
    this.birdsApi.getBirds()
    .then(birds => {
      this.loading = false
      if (birds != null) {
        this.birds = birds
      } else {
        alert('No se pudo cargar la lista de aves!')
      }
    })
  }

  goToDetailBird(bird: Bird) {
    this.router.navigate(['/birds', bird.id])
  }

  gotoBirdAdd() {
   this.router.navigate(['/birds/add']) 
  }
}
