import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Router } from '@angular/router'
import { Location } from '@angular/common';

import { Bird } from '../../shared/models/bird'
import { BirdSight } from '../../shared/models/bird-sight'
import { BirdsApiService } from '../birds-api.service'


@Component({
  selector: 'app-bird-sights-add',
  templateUrl: './bird-sights-add.component.html',
  styleUrls: ['./bird-sights-add.component.scss']
})
export class BirdSightsAddComponent implements OnInit {

  loading = false

  bird: Bird = {} as Bird
  sight: BirdSight = {} as BirdSight

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private router: Router,
    private birdsApi: BirdsApiService,    
  ) { }

  ngOnInit() {
    // Recuperamos la información del ave de la página anterior
    this.bird = history.state.bird

    // Obtenemos la posición para rellenar los campos de localización
    this.getPosition()
  }

  getPosition() {
    navigator.geolocation.getCurrentPosition(position => {
      this.sight.lat = position.coords.latitude
      this.sight.long = position.coords.longitude
    })    
  }

  saveSight() {
    this.loading = true
    this.birdsApi.addBirdSight(this.bird.id, this.sight).then(ok => this.saveSightResult(ok))
  }

  saveSightResult(ok) {
    this.loading = false
    if (ok) {
      alert('Avistamiento guardado correctamente')
    } else {
      alert('¡No se pudo guardar el avistamiento!')
    }
    this.location.back()
  }
}
