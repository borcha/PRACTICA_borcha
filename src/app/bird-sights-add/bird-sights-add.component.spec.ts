import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BirdSightsAddComponent } from './bird-sights-add.component';

describe('BirdSightsAddComponent', () => {
  let component: BirdSightsAddComponent;
  let fixture: ComponentFixture<BirdSightsAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BirdSightsAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BirdSightsAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
