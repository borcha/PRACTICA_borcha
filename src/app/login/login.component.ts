import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'

import { BirdsApiService } from '../birds-api.service'

@Component({
  selector: 'birdwatch-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loading = false

  username: string
  password: string

  constructor(
    private router: Router,
    private birdsApi: BirdsApiService,
   ) {}

  ngOnInit() {
    this.birdsApi.logout()
  }

  login () {
    this.loading = true
    this.birdsApi.login(this.username, this.password).then(r => this.loginResult(r))
    return false // preventDefault
  }

  loginResult(logged) {
    this.loading = false
    if (logged) {
      this.router.navigate(['/']);
    } else {
      alert('Error de autenticación')
    }
  }
}
