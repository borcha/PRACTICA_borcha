import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
  }

  goToBirds() {
    this.router.navigate(['/birds']);
    return false;
  }

  goToBirdAdd() {
    this.router.navigate(['/birds/add']);
    return false;
  }

  goToInfo() {
    this.router.navigate(['/info']);
    return false;
  }
}
