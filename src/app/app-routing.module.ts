import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { BirdsListComponent } from './birds-list/birds-list.component';
import { BirdDetailsComponent } from './bird-details/bird-details.component';
import { BirdSightsAddComponent } from './bird-sights-add/bird-sights-add.component';
import { BirdAddComponent } from './bird-add/bird-add.component';
import { InfoComponent } from './info/info.component';

const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'login', component: LoginComponent },
  { path: 'birds', component: BirdsListComponent },
  { path: 'birds/add', component: BirdAddComponent },
  { path: 'birds/:id', component: BirdDetailsComponent },
  { path: 'birds/:id/sights/add', component: BirdSightsAddComponent },
  { path: 'info', component: InfoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
