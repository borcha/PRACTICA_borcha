import { Injectable } from '@angular/core'

import { Bird } from 'src/shared/models/bird'
import { BirdDetail } from 'src/shared/models/bird-detail'
import { BirdSight } from 'src/shared/models/bird-sight'


const BASE_URI = 'http://dev.contanimacion.com/birds/public'

@Injectable({
  providedIn: 'root'
})
export class BirdsApiService {

  constructor() { }

  // Recuperamos el usuario logado del localstorage
  getLoggedUser() {
    return JSON.parse(localStorage.getItem('loggedUser'))
  }

  // El logout borramos las credenciales
  logout() {
    localStorage.removeItem('loggedUser')
  }

  // Si conseguimos logarnos guardamos las credenciales en localstorage
  async login(user, password) {
    let result = await POST('login', {user, password}) as any
    
    if (result.status != 'OK') {
      console.error(result.message)
      return false
    }

    localStorage.setItem('loggedUser', JSON.stringify({user, id:result.id}))
    return true
  }

  // Obtener la lista de las aves del usuario logado
  async getBirds() {
    let id = this.getLoggedUser().id
    let result = await GET(`getBirds/${id}`) as any

    if (!(result instanceof Array)) {
      console.error(result.message)
      return null
    }

    // Realizamos la conversión del json recibido a nuestros tipos de objetos
    return result.map(bird => ({
      id: bird.id,
      name: bird.bird_name,
      image: bird.bird_image,
      sightings: bird.bird_sightings,
      seen: bird.mine == 1
    } as Bird))
  }

  // Obtenemos los detalles del ave
  async getBird(id: number) {
    let result = await GET(`getBirdDetails/${id}`) as any

    if (!(result instanceof Array) || result.length != 1) {
      console.error(result.message)
      return null
    }

    // En la documentación parecía que devolvía el objeto json directamente
    // pero se está recibiendo un array con un único elemento
    result = result[0]

    // Realizamos la conversión del json recibido a nuestros tipos de objetos
    return {
      id: result.id,
      name: result.bird_name,
      description: result.bird_description,
      image: result.bird_image,
      sightings: result.bird_sightings,
      sightingsList: result.sightings_list.map(sight => sight as BirdSight)
    } as BirdDetail
  }

  // Añadir avistamiento
  async addBirdSight(birdId: number, sight: BirdSight) {
    let result = await POST('addSighting', {...sight, idAve: birdId}) as any

    if (!result.status) {
      console.error(result.message)
      return false
    }

    return true
  }


  // Crear nueva ave
  async addBird(bird: BirdDetail, sight?: BirdSight) {
    let idUser = this.getLoggedUser().id

    // Información mínima del ave
    let birdData = {
      idUser,
      bird_name: bird.name,
      bird_description: bird.description
    }

    // Agregamos la información opcional del avistamiento
    if (bird.seen && sight != null) {
      birdData = {...birdData, ...sight}
    }

    let result = await POST('addBird', birdData) as any
    if (result.status != 'OK') {
      console.error(result.message)
      return false
    }

    return true
  }
}


// Método utilidad para realizar las peticiones ajax
async function POST(service, paramsObj) {
  return await fetch(`${BASE_URI}/${service}/`, {
    method:'POST',
    headers: {'Content-Type':'application/x-www-form-urlencoded'},
    body: new URLSearchParams(paramsObj).toString()
  })
  .then(r => r.json())
  //.then(pause)
  .catch(e => ({ status:'KO', message: e.message }))
}

async function GET(service, paramsObj?) {
  let url = new URL(`${BASE_URI}/${service}`)
  url.search = new URLSearchParams(paramsObj).toString()
  return await fetch(url.toString())
  .then(r => r.json())
  //.then(pause)
  .catch(e => ({ status:'KO', message: e.message }))
}

// Utilidad en tiemo de desarrollo para simular esperas en las llamadas a los servicios web
function pause(x) {
  console.log(new Date().toLocaleTimeString(), 'Retrasando la respuesta')
  return new Promise(resolve => setTimeout(() => resolve(x), 4000))
}