import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Router } from '@angular/router'

import { BirdDetail } from '../../shared/models/bird-detail'
import { BirdsApiService } from '../birds-api.service'

@Component({
  selector: 'app-bird-details',
  templateUrl: './bird-details.component.html',
  styleUrls: ['./bird-details.component.scss']
})
export class BirdDetailsComponent implements OnInit {

  loading = true

  bird: BirdDetail = {} as BirdDetail

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private birdsApi: BirdsApiService,    
  ) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id')
    this.birdsApi.getBird(id)
    .then(bird => {
      this.loading = false
      if (bird != null) {
        this.bird = bird
      } else {
        alert('No se pudo cargar la información del ave!')
      }
    })    
  }

  goToBirdSightAdd() {
    this.router.navigate(['birds', this.bird.id, 'sights', 'add'], {state:{bird:this.bird}})
    return false // preventDefault 
  }
}
