import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { BirdsListComponent } from './birds-list/birds-list.component';
import { BirdDetailsComponent } from './bird-details/bird-details.component';
import { BirdAddComponent } from './bird-add/bird-add.component';
import { BirdSightsAddComponent } from './bird-sights-add/bird-sights-add.component';
import { InfoComponent } from './info/info.component';
import { BaseLayoutComponent } from './base-layout/base-layout.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    BirdsListComponent,
    BirdDetailsComponent,
    BirdAddComponent,
    BirdSightsAddComponent,
    InfoComponent,
    BaseLayoutComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFontAwesomeModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
