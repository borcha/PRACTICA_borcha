import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Location } from '@angular/common';

import { BirdDetail } from '../../shared/models/bird-detail'
import { BirdSight } from '../../shared/models/bird-sight'
import { BirdsApiService } from '../birds-api.service'

@Component({
  selector: 'app-bird-add',
  templateUrl: './bird-add.component.html',
  styleUrls: ['./bird-add.component.scss']
})
export class BirdAddComponent implements OnInit {

  loading = false

  // Mostramos el checkbox de avistamiento desactivado
  bird: BirdDetail = {seen:false} as BirdDetail
  sight: BirdSight = {} as BirdSight

  constructor(
    private location: Location,
    private router: Router,
    private birdsApi: BirdsApiService,        
  ) { }

  ngOnInit() {
  }

  getPosition() {
    if (this.bird.seen) {
      navigator.geolocation.getCurrentPosition(position => {
        this.sight.lat = position.coords.latitude
        this.sight.long = position.coords.longitude
      })      
    }
  }

  saveBird() {
    this.loading = true
    this.birdsApi.addBird(this.bird, this.sight).then(ok => this.saveBirdResult(ok))
  }

  saveBirdResult (ok) {
    this.loading = false
    if (ok) {
      alert("Ave guardada corectamente")
    } else {
      alert("¡No se pudo guardar el ave!")
    }
    this.location.back()
  }
}
