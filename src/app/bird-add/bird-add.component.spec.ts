import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BirdAddComponent } from './bird-add.component';

describe('BirdAddComponent', () => {
  let component: BirdAddComponent;
  let fixture: ComponentFixture<BirdAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BirdAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BirdAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
