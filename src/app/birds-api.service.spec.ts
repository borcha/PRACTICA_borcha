import { TestBed } from '@angular/core/testing';

import { BirdsApiService } from './birds-api.service';

describe('BirdsApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BirdsApiService = TestBed.get(BirdsApiService);
    expect(service).toBeTruthy();
  });
});
